from django.urls import path
from AE_NewP.views.user import UserRegisterAPIView , ClockinEmployee, ClockoutEmployee
from AE_NewP.views.home import DashboardAdminList, DashboardAdminListNew
from django.conf.urls import url
from rest_framework_swagger.views import get_swagger_view
from django.conf import settings

schema_view = get_swagger_view(title='Attendance API')

urlpatterns = [
    # path('', schema_view),
    path('', DashboardAdminListNew.as_view(), name='admin_list'),
    path('dadm/', DashboardAdminList.as_view(), name='employee_list'),
    path('register/', UserRegisterAPIView.as_view(),name='register'),
    path('clockin/', ClockinEmployee.as_view(),name='clockin'),
    path('clockout/', ClockoutEmployee.as_view(), name='clockout'),
    # path('home/', home.as_view(), name='home'), 
    # path('clockout/', )
]
