from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.conf import settings

from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.response import Response
from rest_framework.views import APIView
from django.http import HttpResponse
from rest_framework.generics import (CreateAPIView, GenericAPIView,
                                     RetrieveAPIView,ListCreateAPIView)
from AE_NewP.models.user import (Attendance, DateEmployee, Department,
                                 UserAttend)
from django.views.generic import TemplateView,ListView      
from django.shortcuts import get_object_or_404                           
import request


    
class DashboardAdminList(TemplateView):
    # renderer_classes = [TemplateHTMLRenderer]
    template_name = 'dashboard_admin.html'

    # def get(self, request, pk):
    #     employee = get_object_or_404(UserAttend, pk=pk)
    #     serializer = ProfileSerializer(employee)
    #     return Response({'serializer': serializer, 'employee': employee})

    # def post(self, request, pk):
    #     employee = get_object_or_404(UserAttend, pk=pk)
    #     serializer = ProfileSerializer(employee, data=request.data)
    #     if not serializer.is_valid():
    #         return Response({'serializer': serializer, 'employee': employee})
    #     serializer.save()
    #     return redirect('employee-list')

class DashboardAdminListNew(TemplateView):
    template_name = 'dashboard_adminnew.html'