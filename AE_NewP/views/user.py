import time
import uuid
from datetime import datetime

import request
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import authenticate, get_user_model
from django.contrib.auth import login as django_login
from django.contrib.auth import logout as django_logout
from django.contrib.auth.models import AbstractUser
from django.contrib.sessions.models import Session
from django.contrib.sites.shortcuts import get_current_site
from django.core.exceptions import FieldDoesNotExist, ObjectDoesNotExist
from django.http import HttpResponse
from django.shortcuts import redirect, render
from django.template.loader import render_to_string
from django.utils.decorators import method_decorator
from django.utils.translation import gettext
from django.utils.translation import gettext_lazy as _
from django.utils import timezone
from django.views.decorators.debug import sensitive_post_parameters
from rest_framework import authentication, serializers, status
from rest_framework.decorators import api_view
from rest_framework.generics import (CreateAPIView, GenericAPIView,
                                     RetrieveAPIView)
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.validators import ValidationError
from rest_framework.views import APIView

from AE_NewP.models.user import (Attendance, DateEmployee, Department,
                                 UserAttend)
from AE_NewP.serializers.serializers import (ClockinSerializer,
                                             ClockoutSerializer,
                                             UserAttendSerializer)


#UserRegisterAPIView - START
# Description: User Register and receives a POST with a user's to check 'UDID' & 'Email' Validation,
#              if valid then go to next step and check 'Password' to give acces user to login into account
#              for now didn't have return token authenticate or verification, only give an acces to account
# Author     : Kevin Gifari Adriansyah
# Created_ON : Monday, 28 January 2018.
# Updated_ON : Monday, 4 February 2018.
# Version    : 1.0:1
class UserRegisterAPIView(CreateAPIView):
    serializer_class = UserAttendSerializer

    def post(self,request):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            email = serializer.validated_data.get("email")
            device_key = serializer.validated_data.get("device_key")
            password = serializer.validated_data.get("password")
            employee, created = UserAttend.objects.get_or_create(
                username=email, password=password, email=email, device_key=device_key
            )
            if not created:
                return Response({'error': 'User {} already exists'.format(email)}, status=status.HTTP_302_FOUND)
            employee.save()
            return Response({'message': 'User {} has been created'.format(email)}, status=status.HTTP_201_CREATED)
        return Response({'error': serializer.errors}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
# UserRegisterAPIView - End


class ClockinEmployee(GenericAPIView):
    serializer_class = ClockinSerializer

    def post(self,request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)

        if serializer.is_valid():
            device_key=serializer.validated_data.get("device_key")
            # user = UserAttend.objects.get('id')
            try:
                user = UserAttend.objects.get(device_key=device_key)
            except:
                 return Response({'message': 'Your Device Key is not registered please register to use Attendance Apps.'},status=status.HTTP_404_NOT_FOUND) 
            if not user:
                device_key = UserAttend.objects.filter("device_key")
                return Response({'message': 'User Device {} is founded '.format(device_key)}, status=status.HTTP_302_FOUND)
            
            date=datetime.today()
            timestamp=datetime.now().timetz()
            ae=Attendance.objects.create(username=user,date=datetime.today(),timestamp=datetime.now(),statusattendance=Attendance.ClockIn)
            if date and timestamp != datetime.today().date():
                return Response({'message': 'Clock In user {} is only once a day'.format(date)},status=status.HTTP_400_BAD_REQUEST)            
            else:    
                ae.save()
                return Response({'message': 'Device Key is Match {} Clock in sucessful'.format(device_key)},status=status.HTTP_302_FOUND)    
        return Response({'error': serializer.errors}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

class ClockoutEmployee(GenericAPIView):
    serializer_class = ClockoutSerializer

    def post(self,request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)

        if serializer.is_valid():
            device_key=serializer.validated_data.get("device_key")
            # user = UserAttend.objects.get('id')
            try:
                user = UserAttend.objects.get(device_key=device_key)
            except:
                return Response({'message': 'Your Device Key is not registered please register to use Attendance Apps.'},status=status.HTTP_404_NOT_FOUND) 
            ae=Attendance.objects.create(username=user,date=datetime.today(),timestamp=timezone.datetime.now(),statusattendance=Attendance.ClockOut)
            if not user:
                device_key = UserAttend.objects.filter("device_key")
                return Response({'message': 'User Device {} is founded '.format(device_key)}, status=status.HTTP_302_FOUND)
            ae.save()
            return Response({'message': 'Device Key is Match {} Clock out successful'.format(device_key)},status=status.HTTP_302_FOUND)    
        return Response({'error': serializer.errors}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        
# class UserCheckAPIView(APIView):
    # def post(self,request,*args, **kwargs):
    #     usernamep=request.data.get("username")
    #     emailp=request.data.get("email")
    #     passwordp=request.data.get("password")
    #     device_keyp=request.data.get("device_key")
    #     statep=request.data.get("state")        
    #     print(request.data)
    #     result = self.reguservalidate(usernamep, passwordp, emailp, device_keyp, statep)
    #     if result is not True:
    #         UserAttend.objects.filter(
    #             no_udid=uuid.uuid1(),
    #             username=usernamep,
    #             email=emailp,
    #             password=passwordp,
    #             device_key=device_keyp,
    #             state=statep,
    #         )
    #     return Response(data={"result":result})

    # def reguservalidate(self, usernamep, passwordp, emailp, device_keyp, statep):
    #     emailp=UserAttend.objects.filter(email=emailp).last()
    #     device_keyp=UserAttend.objects.filter(device_key=device_keyp).last()
    #     statep=UserAttend.objects.filter(state=statep).last()
    #     passwordp=UserAttend.objects.filter(password=passwordp).last()
    #     if emailp is not None and device_keyp and passwordp:
    #         self.UserAttend_cache = authenticate(self.request, email=emailp, device_key=device_keyp,password=passwordp)
    #         if self.UserAttend_cache is None:
    #             try:
    #                 user_temp = (
    #                     UserAttend.objects.get(email=emailp).filter(email=emailp), #(database=parameter/variable)
    #                     UserAttend.objects.get(device_key=device_keyp).filter(device_key=device_keyp),
    #                     UserAttend.objects.get(password=passwordp).filter(password=passwordp),
    #                     UserAttend.objects.get(state=statep).filter(state=statep),
    #                 )
    #                 # UserAttend.objects.get(username=usernamep) 
    #                 # UserAttend.objects.get(password=passwordp).filter(password=passwordp)
    #             except:
    #                 user_temp = None

    #                 if user_temp is not None and user_temp.check_password(passwordp):
    #                     AuthenticationForm.confirm_login_allowed(user_temp)
    #                 else:
    #                     raise forms.ValidationError(
    #                         AuthenticationForm.error_messages['invalid_login'],
    #                         code='invalid_login',
    #                         params={'email': UserAttend.objects.get(email=emailp).verbose_name}
    #                     )
    #                 return False
    #             return True

# Login Authentication
# class UserLoginAPIView(APIView):
    # def login_attendance(request):
    #     if request.POST:
    #         UserAttend = authenticate(device_key=request.POST['device_key'],email=request.POST['email'],password=request.POST['password'])
    #         if UserAttend is not None:
    #             if UserAttend.is_active:
    #                 try:
    #                     account = UserAttend.objects.get (account = UserAttend.id)
    #                     login (request, UserAttend)

    #                     request.session ['staf_id'] = account.UserAttend.id
    #                     request.session ['email'] = account.UserAttend.email
    #                     request.session ['no_udid'] = account.UserAttend.no_udid
    #                 except:
    #                     messages.add_message (request, messages.INFO, 'This account is not yet connected with employee data, please contact administrator')
    #                 return redirect ('/')
    #             else:
    #                 messages.add_message (request, messages.INFO, 'User unverified')
    #         else:
    #             messages.add_message(request, messages.INFO, 'Your Account is wrong')
    #     return render (request, 'login.html')

    # def logout_attendance (request):
    #     logout (request)
    #     return redirect ('/ login /')
