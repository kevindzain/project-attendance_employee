
from django.db import models
from django.contrib import admin
from django.contrib.auth.models import AbstractUser #, PermissionsMixin
from django.contrib.auth.models import UserManager as DjangoManager
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext,ugettext_lazy as _
from django.conf import settings

from rest_framework.authtoken.models import Token as DefaultTokenModel

import rest_framework.authtoken.models
import uuid
import datetime

class UserM(DjangoManager):
    # def _create_user(self, email, password, **extra_fields):
        # """
        # Create and save a user with the given username, email, and password.
        # """
        # if not username:
        #     raise ValueError('The given username must be set')
        # email = self.normalize_email(email)
        # username = self.model.normalize_username(username)
        # user = self.model(username=username, email=email, **extra_fields)
        # user.set_password(password)
        # # user.save(using=self._db)
        # return user

    # def create_user(self, email=None, password=None, **extra_fields):
        # extra_fields.setdefault('no_udid', uuid.uuid1())
        # extra_fields.setdefault('is_active', False)
        # extra_fields.setdefault('is_staff', False)
        # extra_fields.setdefault('is_superuser', False)
        # return self._create_user(username, email, password, **extra_fields)
        
        # user = self._create_user(username, email, password, **extra_fields)
        # department=Department.objects.get(department_id=department_id)
        # user.department_name=department
        # user.save()
        # return user

    # def create_superuser(self, email, password, **extra_fields):
        # extra_fields.setdefault('no_udid', uuid.uuid1())
        # extra_fields.setdefault('is_staff', True)
        # extra_fields.setdefault('is_superuser', True)

        # if extra_fields.get('is_staff') is not True:
        #     raise ValueError('Superuser must have is_staff=True.')
        # if extra_fields.get('is_superuser') is not True:
        #     raise ValueError('Superuser must have is_superuser=True.')

        # return self._create_user(username, email, password, **extra_fields)
        # user = self._create_user(username, email, password, **extra_fields)
        # department=Department.objects.get(department_id=department_id)
        # user.department_name=department
        # user.save()
        # return user

    # use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """Create and save a User with the given email and password."""
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        """Create and save a regular User with the given email and password."""
        extra_fields.setdefault('no_udid', uuid.uuid1())
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        """Create and save a SuperUser with the given email and password."""
        extra_fields.setdefault('no_udid', uuid.uuid1())
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)


class UserAttend(AbstractUser):
    
    nik = models.CharField(max_length=50, blank=True, null=True)
    device_key = models.CharField(max_length=255)
    state = models.BooleanField(_("Active"),default=True,
    help_text=('Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'), blank=True, null=True)
    department_name = models.CharField(max_length=40, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.email

class Department(models.Model):
    department_id = models.CharField(primary_key=True,max_length=50,null=False)
    department_name = models.ForeignKey(UserAttend, on_delete=models.CASCADE)

    def _str_(self):
        return self.department_id

# class TokenModel():
#     token = models.CharField(max_length=30)
#     user = models.OneToOneField(
#         UserAttend,
#         on_delete=models.CASCADE,
#     )


class Attendance(models.Model):
    ClockIn='clock in'
    ClockOut='clock out'
    StatusClock =(
        ('clock in','Clock in'),
        ('clock out','clock out'))

    date = models.DateField(auto_now_add=True)
    username = models.ForeignKey(UserAttend, on_delete=models.CASCADE)
    timestamp = models.DateTimeField(auto_now_add=True)
    statusattendance = models.CharField(max_length=50, default=ClockIn ,choices=StatusClock)

    def _str_(self):
        return self.date

class DateEmployee(models.Model):
    day = models.DateField(auto_now_add=True)
    month = models.CharField(max_length=50)
    year = models.CharField(max_length=50)
    status = models.CharField(max_length=50)
    date = models.ForeignKey(Attendance, on_delete=models.CASCADE)
    def _str_(self):
        return self.day
 