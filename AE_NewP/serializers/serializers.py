from rest_framework import serializers
from AE_NewP.models.user import UserAttend,Attendance
from datetime import datetime
from rest_framework import status
from rest_framework.response import Response

class UserAttendSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserAttend
        fields = ('email','password','device_key')

class ClockinSerializer(serializers.Serializer):
    device_key = serializers.CharField()

    # def create(self):
    #     device_key=UserAttend.objects.get("device_key").filter()
    #     AE=Attendance(date=datetime.date.today(),timestamp=datetime.now(),statusattendance='Clock In')
    #     AE.save()
    #     return Response({'message': 'User {} has been created'.format(device_key)}, status=status.HTTP_201_CREATED)
    # def update(self, instance, validated_data):
    #     instance.email = validated_data.get('email', instance.email)
    #     instance.password = validated_data.get('password', instance.password)
    #     instance.save()
    #     return instance

class ClockoutSerializer(serializers.Serializer):
    device_key = serializers.CharField()

    # def create(self):
    #     device_key=UserAttend.objects.get("device_key").filter()
    #     AE=Attendance(date=datetime.date.today(),timestamp=datetime.now(),statusattendance='Clock In')
    #     AE.save()
    #     return Response({'message': 'User {} has been created'.format(device_key)}, status=status.HTTP_201_CREATED)

class AdminDashboard(serializers.ModelSerializer):
    class Meta:
        model = UserAttend
        fields = ('email','password','device_key','nik','department_name')