from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from django.urls import reverse
# from AE_NewP.models.user import UserToken 

# class Mailer:
#     context = {}
#     subject = ""
#     template = ""
#     email_receiver_list = []

#     def send(self, attachments=None):
#         if not (bool(self.subject) and bool(self.template) and bool(self.email_receiver_list)):
#             raise NotImplementedError(
#                 'Function that you are using not correctly configured yet.\n'
#                 '\ncontext = {}'
#                 '\nsubject = {}'
#                 '\ntemplate = {}'
#                 '\nemail_receiver_list = {}'.format(
#                     self.context,
#                     self.subject,
#                     self.template,
#                     self.email_receiver_list
#                 )
#             )
#         subject = self.subject
#         message_string = render_to_string(
#             self.template,
#             self.context
#         )
#         message = EmailMessage(
#             subject,
#             message_string,
#             to=self.email_receiver_list
#         )
#         message.content_subtype = 'html'
#         if attachments:
#             for attach in attachments:
#                 message.attach_file(attach)
#         message.send()


# class AuthenticationMailer(Mailer):
    
#     def __init__(self, request):
#         self.request = request

#     def email_verification(self, user):
#         self.template = 'emails/authentication/email_verification.html'
#         self.subject = "Verify Your Email | Attendance Employee"
#         self.email_receiver_list = [user.email]
#         path = reverse(
#             'verify_email_get',
#             kwargs={'token': UserToken.objects.get(user=user, usage=UserToken.EMAIL_VERIFICATION).token}
#         )
#         self.context = {
#             'email': user.email,
#             # 'logo': self.request.build_absolute_uri(static('images/frisidea.jpg')),
#             'url': "{}{}".format(self.request.get_host(), path),
#             'full_name': user.get_full_name(),
#         }
#         self.send()