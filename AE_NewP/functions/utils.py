from six import string_types
from importlib import import_module
# from AE_NewP.models.user import UserAttend, TokenModel

def import_callable(path_or_callable):
    if hasattr(path_or_callable, '__call__'):
        return path_or_callable
    else:
        assert isinstance(path_or_callable, string_types)
        package, attr = path_or_callable.rsplit('.', 1)
        return getattr(import_module(package), attr)

def default_create_token(token_model, UserAttend, serializer):
    token, _ = token_model.object.get_or_create(UserAttend=UserAttend)
    return token

