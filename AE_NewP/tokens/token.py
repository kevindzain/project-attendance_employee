# from django.contrib.auth.tokens import PasswordResetTokenGenerator
# from django.utils import six
# from AE_NewP.models.user import UserAttend

# class TokenGenerator(PasswordResetTokenGenerator):
#     def _make_hash_value(self, UserAttend, timestamp):
#         return (
#             six.text_type(UserAttend.pk) + six.text_type(timestamp) + 
#             six.text_type(UserAttend.is_active)
#         )

# account_activation_token = TokenGenerator()

""" Second part is to create a token generator for 
the email confirmation url. 
We can inherit the built-in 
PasswordResetTokenGenerator to make it easier. """